	Spring Security 3.x+ 标签库扩展

	<%@ taglib prefix="sec-ext" uri="http://www.springframework.org/security/ext-tags" %>
    <sec-ext:user>
        登录用户,包括记住我用户
    </sec-ext:user>
    <sec-ext:guest>
        游客
    </sec-ext:guest>
    <sec-ext:authenticated>
        登录用户,不包括记住我用户
    </sec-ext:authenticated>
    <sec-ext:notAuthenticated>
        游客,记住我用户
    </sec-ext:notAuthenticated>
    <sec-ext:principal>
        登陆时为用户名
        未登录时 为配置的
            *anonymous帐户名
            * anonymousUser 和 guest(不区分大小写) 时 展示为 游客
    </sec-ext:principal>

    
						By 筱龙缘