package org.springframework.security.taglibs.extension;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

/**
 * 自定义标签上下文获取接口
 * Created by LongYuan on 2015/1/27.
 */
public interface TagContextTag  {

    /**
     * This method allows subclasses to provide a way to access the ServletRequest according to the rendering
     * technology.
     */
    public  ServletRequest getRequest();

    /**
     * This method allows subclasses to provide a way to access the ServletResponse according to the rendering
     * technology.
     */
    public  ServletResponse getResponse();

    /**
     * This method allows subclasses to provide a way to access the HttpSession according to the rendering
     * technology.
     */
    public HttpSession getSession();
    /**
     * This method allows subclasses to provide a way to access the ServletContext according to the rendering
     * technology.
     */
    public  ServletContext getServletContext();

    /**
     * 由具体标签提供页面上下文
     * @return
     */
    public PageContext getPageContext( );

}
