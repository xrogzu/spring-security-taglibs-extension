package org.springframework.security.taglibs.extension;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import java.io.IOException;

/**
 *
 * Created by LongYuan on 2015/1/27.
 */
public class UserTag extends SecureTag  {
    private static final Logger log = LoggerFactory.getLogger(UserTag.class);

    private PageContext pageContext;

    public UserTag(){
        
    }


    @Override
    public int onDoStartTag() throws JspException {
        try {
            boolean isAuthenticated = authorizeUsingAccessExpression("isAuthenticated()");
            if(isAuthenticated) {
                if(log.isTraceEnabled()) {
                    log.trace("Authentication exists or has a known identity (aka \'principal\').  Tag body will not be evaluated.");
                }
                return EVAL_BODY_INCLUDE;
            } else {
                if(log.isTraceEnabled()) {
                    log.trace("Authentication does not exist or does not have a known identity (aka \'principal\').  Tag body will be evaluated.");
                }

                return SKIP_BODY;
            }
        } catch (IOException e) {
            throw new JspException(e);
        }
    }

    @Override
    public void setPageContext(PageContext pageContext) {
        this.pageContext = pageContext;
    }

    @Override
    public PageContext getPageContext() {
        return this.pageContext;
    }

}
